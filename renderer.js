// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.


//vars tcp
const fs = require('fs');
var net = require('net');
var TCPsock = new net.Socket();

//vars que reciben las entradas analogicas del micro
var totalInt = [null,null];
var high8bits = [null,null];
var low8bits =[null,null];

//vars que reciben las salidas analogicas del micro
var analogOutInt = null;
var analogOuthigh = null;
var analogOutlow = null;

//vars que envian las salidas analogicas del micro
var setAnalogOutLow = null;
var setAnalogOutHigh = null;


var nextLabel = 31;
var polling;
var Channelreadable = [false,false];
var ready = true;

//=========================================================
// EVENTOS CONEXIONES
//=========================================================
TCPsock.on('error', function () {
    $('#connectionMsg').text('No se ha podido conectar con la direccion solicitada, compruebe los datos y el estado del servidor');
    
    //bloquea los inputs del panel
    $('.slider').siblings().prop('disabled',true);
    $('.slider').addClass('disabled-slider');
    
    $('#sendAnalogVoltage').prop('disabled',true);
    $('#channel-selector').prop('disabled',true);
    $('#setVoltageValue').prop('disabled',true);
    
    //si no puede conectar
     $("#mainZone>div").addClass("hide");
     $("#panelConfig").removeClass("hide"); 
    
    //termina el muestreo de el estado del micro
    clearInterval(polling);
    resetVars();
});

TCPsock.on('data', function(data) {
    
    var pointer = 0; 
    do
    {
        switch(data[pointer + 0])
        {
            //caso pulsador
            case 0:
                if(data[pointer + 2]!=$('#pul'+data[pointer + 1]+'ON').data('state')) //si el nuevo estado es distinto se cambian la imagen que se muestra
                {
                    $('#pul'+data[pointer + 1]+'ON').toggleClass('hide');
                    $('#pul'+data[pointer + 1]+'OFF').toggleClass('hide');

                    $('#pul'+data[pointer + 1]+'ON').data('state',data[pointer + 2]);
                }
                break;

            //caso digital output write
            case 2:    
                //si se activa esta etiqueta es que se ha recibido un asentimiento, en vez de usar
                //un break, se deja pasar al siguiente caso para que actualize instantaneamente el nuevo estado de el led 
            //caso digital output read
            case 1:
                if(data[pointer + 2]!=$('#led'+data[pointer + 1]+'ON').data('state'))
                {
                    $('#led'+data[pointer + 1]+'ON').toggleClass('hide');
                    $('#led'+data[pointer + 1]+'OFF').toggleClass('hide');

                    $('#led'+data[pointer + 1]+'ON').data('state',data[pointer + 2]);   
                    
                    if($('#led'+data[pointer + 1]+'ON').data('state')==1)
                    {
                        $('#switch'+data[pointer + 1]).prop('checked',true);
                    }
                    else
                    {
                        $('#switch'+data[pointer + 1]).prop('checked',false);
                    }
                }
                break;

            //caso byte input read Analog    
            case 3:
                //comprueba si no hay parte alta guardada
                if (high8bits[data[pointer + 1]-1]==null)
                {
                    high8bits[data[pointer + 1]-1] = data[pointer + 2];
                    //solicita otra vez el valor, en este caso el micro enviara la oparte baja
                    check(3,data[pointer + 1],TCPsock);
                }
                else
                {
                    low8bits[data[pointer + 1]-1] = data[pointer + 2];
                    totalInt[data[pointer + 1]-1] = high8bits[data[pointer + 1]-1]<<8;
                    totalInt[data[pointer + 1]-1] |= low8bits[data[pointer + 1]-1];
                    high8bits[data[pointer + 1]-1] = low8bits[data[pointer + 1]-1] = null;
                }

                if($('#CH'+data[pointer + 1]).prop('checked')==false)
                {
                    //canal desactivado
                    totalInt[data[pointer + 1]-1] = 'NaN'
                }
                
                //si se tiene una muestra de ambos canales, se dibuja en la interfaz
                if(totalInt[0] != null && totalInt[1]!=null)
                {
                    addGraphData(VoltChart,nextLabel,totalInt);
                    totalInt[0]=totalInt[1]=null;
                    nextLabel++;
                }
                break;
            //caso byte output read Analog    
            case 4:
                if (analogOuthigh==null)
                {
                    analogOuthigh = data[pointer + 2];
                    //solicita otra vez el valor, en este caso el micro enviara la parte baja
                    check(4,data[pointer + 1],TCPsock);
                }
                else
                {
                    analogOutlow = data[pointer + 2];
                    analogOutInt = analogOuthigh<<8;
                    analogOutInt |= analogOutlow;
                    analogOuthigh = analogOutlow = null;
                    
                    //actualizar en DOM
                    $("#Current-value").text("Valor actual: "+analogOutInt/1000 +" V");
                    analogOutInt = null;
                    ready = true;
                }
                
                break;

            //caso byte output write Analog
            case 5:
                //envia la parte baja, si no lo ha hecho ya
                if(setAnalogOutLow!=null)
                {
                    send(5,data[pointer + 1],setAnalogOutLow,TCPsock);
                    setAnalogOutLow = null;
                }
                break;
        }
        pointer +=3 //quita los 3 primero datos del buffer
        
    }while(data[pointer]!=null);
    });


//=======================================================
//FUNCION QUE CONECTA REPETIDAMENTE CON EL SERVIDOR PARA PEDIR ACTUALIZACIONES
//=======================================================

function polling(){
    //estado de los led

    check(1,1,TCPsock);
    check(1,2,TCPsock);
    check(1,3,TCPsock);
    check(1,4,TCPsock);
    
    //estado de los pulsadores
   
    check(0,1,TCPsock);
    check(0,2,TCPsock);
    check(0,3,TCPsock);
    check(0,4,TCPsock);
	//estado de la entrada analogica
    //comprueba que el canal este encendido y 
    //que no está en medio de un envío ya
    if($('#CH1').prop('checked') && totalInt[0]==null)
    {
        check(3,1,TCPsock);
    }
    else
    {
        totalInt[0] = 'NaN';
    }

    if($('#CH2').prop('checked') && totalInt[1]==null)
    {
        check(3,2,TCPsock);
    }
    else
    {
        totalInt[1] = 'NaN';
    }
    

	//estado de la salida analogica solo muestra un canal que es el seleccionado
    if($('#channel-selector').val()==1 && Channelreadable[0] && ready)
    {
        ready=false;
	    check(4,1,TCPsock);
    }
    else if($('#channel-selector').val()==2 && Channelreadable[1] && ready)
    {
        ready=false;
	    check(4,2,TCPsock);
    }
}

//orden de modificar las salidas digitales(led)
function controlLed(address,state,socket)
{
    send(2,address,state,socket);
}

//orden de modificar las salidas analogicas
function controlAnalogOut(address,level,socket)
{
    setAnalogOutLow = level & 0xFF;
    setAnalogOutHigh = (level>>>8) & 0xFF;
    send(5,address,setAnalogOutHigh,socket);
    //y esperar por ack
    setAnalogOutHigh = null;
}


//========================================================
//FUNCIONES
//========================================================


//solicita la info de el elemento de la dirección indicada
function check(command,address,socket)
{
    var buffer = Buffer(3);
    buffer[0] = command;
    buffer[1] = address;
    buffer[2] = 0; //irrelevante

    socket.write(buffer);
}

//envia la info al elemento con la direccion indicada
function send(command,address,info,socket)
{
    var buffer = Buffer(3);
    buffer[0] = command;
    buffer[1] = address;
    buffer[2] = info;
    
    socket.write(buffer);
}

//Añadir a la gráfica
//graph -> grafica objetivo
//xlabel -> nombre del proximo valor del eje x
//data -> nuevo valor de y para cada linea
function addGraphData(chart,xlabel,data)
{
    //añadir ultimos datos
    chart.data.labels.push(xlabel);
    chart.data.datasets[0].data.push(data[0]);
    chart.data.datasets[1].data.push(data[1]);
    
    //eliminar datos más viejos
    chart.data.labels.shift();
    chart.data.datasets[0].data.shift();
    chart.data.datasets[1].data.shift();
    chart.update();
}

//reinicia todas las variables en caso de desconexion
function resetVars()
{
    //vars tcp
    TCPsock = new net.Socket();

    //vars que reciben las entradas analogicas del micro
    totalInt = [null,null];
    high8bits = [null,null];
    low8bits =[null,null];

    //vars que reciben las salidas analogicas del micro
    analogOutInt = null;
    analogOuthigh = null;
    analogOutlow = null;

    //vars que envian las salidas analogicas del micro
    setAnalogOutLow = null;
    setAnalogOutHigh = null;
}
//====================================================
//INTERACCION DEL USUARIO
//====================================================
$(document).ready(function() {
    
    //autoconectar si es posible
    fs.readFile("assets/tmp/address.txt",function(err,data){
        var address = data.toString("utf-8").split(" ");
        if (address.length == 2)
        {
            $('#IPinput').attr("placeholder",address[0]);
             $('#portInput').attr("placeholder",address[1]);
            $()
            TCPsock.connect(address[1],address[0],function(){
                $('#connectionMsg').text('Se ha conectado con exito');
                //guardar ultima direccion
                fs.writeFile("assets/tmp/address.txt",address[0]+" "+address[1],function(err){
                    console.log(err); 
                });

                //activa los input cuando se logra una conexión
                $('.disabled-slider').siblings().prop('disabled',false);
                $('.disabled-slider').removeClass('disabled-slider');

                $('#sendAnalogVoltage').prop('disabled',false);
                $('#channel-selector').prop('disabled',false);
                $('#setVoltageValue').prop('disabled',false);


                //inicia el muestreo de el estado del micro
                polling = setInterval(polling,100);
            });
        }
        else
        {
            //si no tiene datos te lleva a la pagina de registro
             $("#mainZone>div").addClass("hide");
             $("#panelConfig").removeClass("hide"); 
        }
    });
    
    
    $('#btnconnect').click(function(){
        $('#connectionMsg').text('Conectando');
        tryConnect();
    });
    
    $('#sendAnalogVoltage').click(function(){
        var value = $('#setVoltageValue').val()*1000;
        if (value>4095)
        {
            value=4095;            
        }
        var channel = $('#channel-selector').val();
        //separar en parte alta y baja
        Channelreadable[channel-1] = true; //se empiezan a pedir lecturas de analog al micro
        controlAnalogOut(channel,value,TCPsock);
    });
    
    function tryConnect()
    {

        var IP = $('#IPinput').val();
        if(net.isIP(IP)!=4 && IP!='localhost')
        {
            $('#connectionMsg').text('La IP introducida no es un ip versión 4 válida');         
            return false;   
        }
        var port = $('#portInput').val();
        TCPsock.connect(port,IP,function(){
            $('#connectionMsg').text('Se ha conectado con exito');
            //guardar ultima direccion
            fs.writeFile("assets/tmp/address.txt",IP+" "+port,function(err){
                console.log(err); 
            });
            
            //activa los input cuando se logra una conexión
            $('.disabled-slider').siblings().prop('disabled',false);
            $('.disabled-slider').removeClass('disabled-slider');
            
            $('#sendAnalogVoltage').prop('disabled',false);
            $('#channel-selector').prop('disabled',false);
            $('#setVoltageValue').prop('disabled',false);
            
            
            //inicia el muestreo de el estado del micro
            window.polling = setInterval(polling,100);
        });
    }
    
    $('.ledSlider').click(function(){
        var targetLed = $(this).siblings('input').data('bind');
        var stateLed = $(this).siblings('input').prop('checked');
        if (stateLed)
        {
            controlLed(targetLed,0,TCPsock);
        }
        else
        {
            controlLed(targetLed,1,TCPsock);
        }
    });
    $("#CH1").click(function(){
        totalInt[0]=null;
        
    });
    $("#CH2").click(function(){
        totalInt[1]=null;
    });
});
